Patch status.

This module adds information about patched modules to the available updates
page at admin/reports/updates. This lets you decide what to do with them when
updating.

Patches are detected via two methods:

1) When they're placed in a module's directory,
  and end in the extension ".patch".

2) When they're listed in a PATCHES.txt file.
  See: https://www.drupal.org/node/615570
